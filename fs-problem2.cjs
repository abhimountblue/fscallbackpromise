const fs = require("fs")

function fsProblem2(file) {
  readFile(file)
    .then((lipsumeData) => {
      const upperCase = lipsumeData.toUpperCase()
      return createFile('upperCase.json', upperCase)
    }).then((filePath) => {
      return appendDataInFile(filePath)
    }).then((filepath) => {
      return readFile(filepath)
    }).then((upperCase) => {
      const lowerCase = upperCase.toLowerCase()
      const splitBySentence = lowerCase.split('.')
      return createFile('sentence.json', splitBySentence)
    }).then((filePath) => {
      return appendDataInFile(filePath)
    }).then((sentenceFilePath) => {
      return readFile(sentenceFilePath)
    }).then((sentenceData) => {
      const sentenceArray = JSON.parse(sentenceData)
      const sortedContent = sentenceArray.sort()
      return createFile('sortContent.json', sortedContent)
    }).then((filePath) => {
      return appendDataInFile(filePath)
    }).then((sortedFilePath) => {
      return readFile('filenames.txt')
    }).then((fileNamesData) => {
      const fileNames = fileNamesData.split('\n')
      return deleteFile(fileNames)
    }).then((res) => {
      console.log(res)
    }).catch((err) => {
      console.error(err.message)
    })
}

function createFile(newFile, data) {
  return new Promise((resolve, reject) => {
    const fileAddress = newFile
    fs.writeFile(fileAddress, JSON.stringify(data, null, 2), (err) => {
      if (err) {
        reject(err)
      } else {
        console.log(`this is create`)
        resolve(fileAddress)
      }
    })
  })
}

function appendDataInFile(appendData) {
  return new Promise((resolve, reject) => {
    const path = './' + appendData + '\n'
    fs.appendFile("filenames.txt", path, (err) => {
      if (err) {
        reject(err)
      } else {
        console.log(`this is append`)
        resolve(appendData)
      }
    })
  })
}

function readFile(file) {
  return new Promise((resolve, reject) => {
    fs.readFile(file, "utf-8", (err, lipsumFileData) => {
      console.log(`this is read`)
      if (err) {
        reject(err)
      } else {
        resolve(lipsumFileData)
      }
    })
  })
}


function deleteFile(fileList) {
  return new Promise((resolve, reject) => {
    let count = 0;
    const fileWillDelete = fileList.length - 2
    for (let file = 0; file < fileList.length - 1; file++) {
      let filePath = fileList[file]
      fs.unlink(filePath, (err) => {
        if (err) {
          reject(err)
        } else {
          count++
          if (count === fileWillDelete) {
            resolve(`all file is deleted`)
          }
        }
      })
    }
  })
}

module.exports = fsProblem2
