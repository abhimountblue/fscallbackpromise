/*
    Problem 1:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Create a directory of random JSON files
        2. Delete those files simultaneously 
    Ensure that the function is invoked as follows: 
        fsProblem1(absolutePathOfRandomDirectory, randomNumberOfFiles)
*/

const fs = require('fs')


function fsProblem1(absolutePathOfRandomDirectory, randomNumberOfFiles) {
    createDirectory(absolutePathOfRandomDirectory)
        .then((res) => {
            console.log(res)
            return createFiles(absolutePathOfRandomDirectory, randomNumberOfFiles)
        }).then((res) => {
            console.log(res)
            return deleteFiles(absolutePathOfRandomDirectory, randomNumberOfFiles)
        }).then((res) => {
            console.log(res)
        }).catch((err) => {
            console.error(err.message)
        })
}

function createDirectory(directory) {
    return new Promise((resole, reject) => {
        fs.mkdir(directory, { recursive: true }, (err) => {
            if (err) {
                reject(err)
            } else {
                resole('Directory Created')
            }
        })
    })
}

function createFiles(absolutePathOfRandomDirectory, randomNumberOfFiles) {
    return new Promise((resole, reject) => {
        let count = 0;
        for (let file = 1; file <= randomNumberOfFiles; file++) {
            let filePath = absolutePathOfRandomDirectory + '/' + file + '.json'
            fs.writeFile(filePath, '', (err) => {
                if (err) {
                    reject(err)
                }
                else {
                    count++
                    console.log('file created',filePath)
                    if (count == randomNumberOfFiles) {
                        resole(`all file is created`)
                    }
                }
            })
        }

    })
}

function deleteFiles(absolutePathOfRandomDirectory, randomNumberOfFiles) {
    return new Promise((resole, reject) => {
        let count = 0;
        for (let file = 1; file <= randomNumberOfFiles; file++) {
            let filePath = absolutePathOfRandomDirectory + '/' + file + '.json'
            fs.unlink(filePath, (err) => {
                if (err) {
                    reject(err)
                }
                else {
                    count++
                    console.log('file created',filePath)
                    if (count == randomNumberOfFiles) {
                        resole(`all file is deleted`)
                    }
                }
            })
        }

    })
}

module.exports = fsProblem1